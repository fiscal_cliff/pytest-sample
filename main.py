import logging
from typing import Union
import sys

logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG, stream=sys.stdout)

logger = logging.getLogger(__name__)

def main():
    logger.info("Started")
    print("Hi, let's check the fib function")
    n = input("What fib number would you like to display?")
    try:
        number = int(n)
        fib_num = fib(number)
        print(f"Your number is {fib_num}")
    except ValueError as exc:
        logger.error(f"Please try entering number instead of this shit {n}", exc_info=exc)
    except Exception as exc:
        logger.error("Unknown error", exc_info=exc)
    
def fib(n: int) -> int:
    if n<=1:
        return n
    return fib(n-1) + fib(n-2)

if __name__ == "__main__":
    main()